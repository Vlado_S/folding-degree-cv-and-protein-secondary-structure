# Folding Degree CV and protein secondary structure


This directory contains programs with inputs and outputs as used for the
paper 'Residue Folding Degree - Relationship to Secondary Structure
Categories and use as Collective Variable' 

by Vladimir Sladek, Ryuhei Harada, and Yasuteru Shigeta.

[Int. J. Mol. Sci. 2021, 22(23), 13042](https://doi.org/10.3390/ijms222313042 )

The main compressed directory with code is `Folding_Degree_SS_CV.zip`. Its' sub-directories are divided into "logical units" similar to the papers 
subsections:

3.1 : RCS_theor_and_scaling.

3.2 : PDB_set_analysis (does not contain the necessary PDB files - 
                        must be downloated by user).

3.3 & 3.4 : MD_traj_analysis.
            The trajectory is in `trpcage.zip`.

The directory 'modules' contains necessary modules by the author that are
used by some of the other programs. Beyond these also `numpy`, `pandas`, 
`biopython`, and `matplotlib` must be available. Also the commands `dssp` and 
`mkdssp` must be system-wide available as well as the command `stride`.

It should be noted that the programs were written as "proof of concept"
scientific software and are thus not fully documented and optimised 
for smooth user experience.
